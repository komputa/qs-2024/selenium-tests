# Try 1
## System prompt
You are a competent software tester who is well versed in metamorphic testing.

## User Prompt
You are responsible to test this application.

We have a web application which presents us with a calendar view that shows different study modules from a university.

There are multiple filters available:
- "Teaching Unit"
- "Study programm"
- "Semester"
- "Module"
- "Lecturer"
- "Room"

Each of those filters will influence the modules shown in the calendar.

A base test would be to select the lecturer "Stefan Bente" and then expect the calendar view to show only his modules.

Another would be to expect the Calendar to only show modules from the teaching unit "Computer Science" if this is the value of the teaching unit filter.

The other filters behave in the same way and multiple can be stacked on top of each other for a fine grained search.

Please generate some metamorphic test cases based on the examples above and all other filter capabilities I've told you about.

## Response (GPT 4)
Sure, here are some metamorphic test cases based on the provided examples:

1. **Initial:** Select "Study programme: Information Technology", expect only modules related to this programme.
   **Follow-up:** Further filter with "Teaching Unit: Computer Science", expect only modules that belong to Computer Science under Information Technology programme.

2. **Initial:** Select "Lecturer: Stefan Bente", expect all modules conducted by him.
   **Follow-up:** Filter out "Semester: Summer", expect only modules conducted by Stefan Bente in Summer semester.

3. **Initial:** Select "Module: Data Structures", expect all instances of this module.
   **Follow-up:** Further filter with "Semester: Fall", expect the Data Structures module only in Fall semester.

4. **Initial:** Select "Room: Lab1", expect all modules conducted in Lab1.
   **Follow-up:** Filter out "Lecturer: Stefan Bente"; expect only the modules in Lab1 conducted by Stefan Bente.

5. **Initial:** Select "Study programme: Computer Science", and "Semester: Summer" - Expect all Computer Science modules in the summer semester.
   **Follow-up:** Now add "Lecturer: Stefan Bente" filter. Expect only modules


# Try 2
## System prompt
You are a software tester who is well versed in metamorphic testing.

## User Prompt
You are responsible to test this application.

We have a web application which presents us with a calendar view that shows different study modules from a university.

There are multiple filters available:
- "Teaching Unit"
- "Study programm"
- "Semester"
- "Module"
- "Lecturer"
- "Room"

Each of those filters will influence the modules shown in the calendar.

Respect the hierarchy of the filters.

A study program is always only part of one teaching unit.
A module only ever has one lecturer.

A base test would be to select the lecturer "Stefan Bente" and then expect the calendar view to show only his modules.

Another would be to expect the Calendar to only show modules from the teaching unit "Computer Science" if this is the value of the teaching unit filter.

The other filters behave in the same way and multiple can be stacked on top of each other for a fine grained search.

Please generate some metamorphic test cases based on the examples above and all other filter capabilities I've told you about.
Also provide the metamorphic relations between the initial test and the follow up.

## Response (GPT 4)
1. Initial Test: Select the lecturer "Stefan Bente", expect the calendar view to show only his modules.

   Metamorphic Follow-Up: Select "Stefan Bente" and "Computer Science" as the teaching unit, expect the calendar view to show only his modules from "Computer Science".

   Metamorphic Relation: Adding an extra filter to the initial test settings should not change the behavior of the original filter. The modules shown should be a subset of the initial test result.

2. Initial Test: Select the teaching unit "Computer Science", expect the calendar view to show modules only from this teaching unit.

   Metamorphic Follow-Up: Select "Computer Science" as the teaching unit and "Second Semester" as the semester, the calendar view should show only "Computer Science" modules from the "Second Semester".

   Metamorphic Relation: Adding an extra semester filter to the test settings should narrow down the results from the initial test. The modules shown should be a subset of the initial test result.

3. Initial Test: Select the "Study program" as "Bachelor of Science", expect the calendar view to show all the modules from this program.

   Metamorphic Follow-Up: Select the "Study program" as "Bachelor of Science" and "Module" as "Data Structures", expect the calendar view to show only the "Data Structures" module from the program.

   Metamorphic Relation: Adding an extra "Module" filter should further subset the results from the initial test. The module shown should be part of the initial "Study program" test result.

4. Initial Test: Select the "Room" as "101", expect the calendar view to show all the modules conducted in this room.

   Metamorphic Follow-Up: Select the "Room" as "101" and "Teaching Unit" as "Mathematics", expect the calendar view to show only "Mathematics" modules conducted in room "101".

   Metamorphic Relation: Adding the "Teaching Unit" filter to the room selection should only show modules that fulfill both conditions. The modules shown should be a subset from the initial test.

5. Initial Test: Select the "Semester" as "First Semester", expect the calendar view to show all the modules from this semester.

   Metamorphic Follow-Up: Select the "Semester" as "First Semester" and "Study program" as "Master of Computer Science", the calendar view should show only "Master of Computer Science" modules from the "First Semester".

   Metamorphic Relation: Adding an extra "Study program" filter to the test settings should narrow down the results from the initial test. The modules shown should be a subset of the initial test result.

# Try 3
## System prompt
You are a software tester who is well versed in metamorphic testing.

## User Prompt
You are responsible to test this application.

We have a web application which presents us with a calendar view that shows different study modules from a university.

There are multiple filters available:
- "Teaching Unit"
- "Study programm"
- "Semester"
- "Module"
- "Lecturer"
- "Room"

Each of those filters will influence the modules shown in the calendar.

Please respect the hierarchy of the filters.

A study program is always only part of one teaching unit.
A module only ever has one lecturer.

A base test would be to select the lecturer "Stefan Bente" and then expect the calendar view to show only his modules.

Another would be to expect the Calendar to only show modules from the teaching unit "Computer Science" if this is the value of the teaching unit filter.

The other filters behave in the same way and multiple can be stacked on top of each other for a fine grained search.

Please generate some metamorphic test cases based on the examples above and all other filter capabilities I've told you about. Also provide the metamorphic relations between the initial test and the follow up.

![Calendar UI](./Screenshot_20240620_131244.png)

The image which shows the UI of the calendar with some pre-selected teaching units that show how the filters react.

## Response (GPT 4o)
Metamorphic testing involves creating follow-up test cases based on transformations of input data while maintaining the relationships between inputs and outputs consistent. Here's a set of metamorphic test cases based on the initial tests you provided:

### Initial Test Case 1
**Description:** Select the lecturer "Stefan Bente" and then expect the calendar view to show only his modules.

#### Follow-up Test Case 1.1
**Metamorphic Relation:** Narrow down further within the "Stefan Bente" selection by specifying the "Room" filter.
- **Initial Input:** Lecturer = "Stefan Bente"
- **Follow-up Input:** Lecturer = "Stefan Bente", Room = "0504"
- **Expected Output:** The calendar shows only modules taught by Stefan Bente in Room 0504.

#### Follow-up Test Case 1.2
**Metamorphic Relation:** Filter modules taught by "Stefan Bente" in a specific semester.
- **Initial Input:** Lecturer = "Stefan Bente"
- **Follow-up Input:** Lecturer = "Stefan Bente", Semester = "Summer 2024"
- **Expected Output:** The calendar shows only modules taught by Stefan Bente in the Summer 2024 semester.

### Initial Test Case 2
**Description:** Select the Teaching Unit "Computer Science" and expect the calendar to show only modules from that teaching unit.

#### Follow-up Test Case 2.1
**Metamorphic Relation:** Stack an additional filter for a specific study program within "Computer Science".
- **Initial Input:** Teaching Unit = "Computer Science"
- **Follow-up Input:** Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)"
- **Expected Output:** The calendar shows only modules from the "Computer Science" teaching unit under the "M.Sc. Digital Sciences (PO1)" study program.

#### Follow-up Test Case 2.2
**Metamorphic Relation:** Add a filter for a specific lecturer within the "Computer Science" teaching unit.
- **Initial Input:** Teaching Unit = "Computer Science"
- **Follow-up Input:** Teaching Unit = "Computer Science", Lecturer = "Stefan Bente"
- **Expected Output:** The calendar shows only modules from the "Computer Science" teaching unit taught by Stefan Bente.

### Initial Test Case 3
**Description:** Select a specific module and expect the calendar to show details for that module.

#### Follow-up Test Case 3.1
**Metamorphic Relation:** Add a filter for a specific semester for the selected module.
- **Initial Input:** Module = "UBICOMP"
- **Follow-up Input:** Module = "UBICOMP", Semester = "Winter 2023"
- **Expected Output:** The calendar shows only the "UBICOMP" module details for the Winter 2023 semester.

### Initial Test Case 4
**Description:** Apply a combination of Teaching Unit and Study Program filters and expect the calendar to show relevant modules.

#### Follow-up Test Case 4.1
**Metamorphic Relation:** Add a Lecturer filter to the current combination of filters.
- **Initial Input:** Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)"
- **Follow-up Input:** Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)", Lecturer = "Stefan Bente"
- **Expected Output:** The calendar shows modules from "Computer Science" teaching unit under "M.Sc. Digital Sciences (PO1)" study program taught by Stefan Bente.

#### Follow-up Test Case 4.2
**Metamorphic Relation:** Add a Room filter to the current combination of filters.
- **Initial Input:** Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)"
- **Follow-up Input:** Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)", Room = "0502"
- **Expected Output:** The calendar shows modules from "Computer Science" teaching unit under "M.Sc. Digital Sciences (PO1)" study program held in Room 0502.

### Initial Test Case 5
**Description:** Filter modules based on room and expect the calendar to show only modules in that room.

#### Follow-up Test Case 5.1
**Metamorphic Relation:** Add a Lecturer filter to the Room filter.
- **Initial Input:** Room = "3111"
- **Follow-up Input:** Room = "3111", Lecturer = "Hans Ludwig Stahl"
- **Expected Output:** The calendar shows only modules in Room 3111 taught by Hans Ludwig Stahl.

### Summary of Metamorphic Relations
1. **Additive Filtering:** Adding more specific filters to narrow down results without changing the initial context.
2. **Hierarchical Validation:** Ensuring that the cascading effect of filters maintains the expected relationship and hierarchy.
3. **Cross-Filtering:** Validating modules across multiple filter dimensions to ensure logical consistency and affect relationships.

Using these metamorphic relations, you can test the robustness of the filtering system and ensure that the calendar view maintains consistency and correctness under various combinations of filter criteria.

## User Prompt
Can you generate test data for the application based on the given information?

## Response
Certainly! Below is some test data for the web application, considering the filters: Teaching Unit, Study Program, Semester, Module, Lecturer, and Room. The data is designed to cover a variety of scenarios to ensure comprehensive testing.

### Test Data

#### Teaching Units
1. **Computer Science**
2. **Electrical Engineering**
3. **Mechanical Engineering**

#### Study Programs (Linked to Teaching Units)
1. **Computer Science**
    - B.Sc. Computer Science (PO2)
    - M.Sc. Digital Sciences (PO1)
2. **Electrical Engineering**
    - B.Sc. Electrical Engineering (PO3)
    - M.Sc. Automated Systems (PO4)
3. **Mechanical Engineering**
    - B.Sc. Mechanical Engineering (PO5)
    - M.Sc. Robotics Engineering (PO6)

#### Semesters
1. Winter 2023
2. Summer 2024

#### Modules (Linked to Study Programs and Lecturers)
1. **Computer Science**
    - B.Sc. Computer Science (PO2)
        - Database Systems (Lecturer: John Doe, Room: 103)
        - Operating Systems (Lecturer: Anna Smith, Room: 205)
    - M.Sc. Digital Sciences (PO1)
        - Advanced Machine Learning (Lecturer: Stefan Bente, Room: 0504)
        - Quantum Computing (Lecturer: Irma Lindt, Room: 3215)

2. **Electrical Engineering**
    - B.Sc. Electrical Engineering (PO3)
        - Circuit Analysis (Lecturer: Mark White, Room: 110)
        - Digital Systems (Lecturer: Laura Grey, Room: 220)
    - M.Sc. Automated Systems (PO4)
        - Control Systems (Lecturer: Jack Black, Room: 315)
        - Signal Processing (Lecturer: Emily Rose, Room: 425)

3. **Mechanical Engineering**
    - B.Sc. Mechanical Engineering (PO5)
        - Thermodynamics (Lecturer: Luke Green, Room: 130)
        - Fluid Mechanics (Lecturer: Sophia Brown, Room: 240)
    - M.Sc. Robotics Engineering (PO6)
        - Robotics (Lecturer: Chris Blue, Room: 345)
        - Mechatronics (Lecturer: Alice White, Room: 455)

### Sample Calendar Entries for Testing

#### Teaching Unit: Computer Science
- **Module:** Advanced Machine Learning
    - **Lecturer:** Stefan Bente
    - **Room:** 0504
    - **Date:** June 18, 2024
    - **Time:** 08:00 - 10:00
    - **Semester:** Summer 2024

- **Module:** Quantum Computing
    - **Lecturer:** Irma Lindt
    - **Room:** 3215
    - **Date:** June 19, 2024
    - **Time:** 09:00 - 11:00
    - **Semester:** Summer 2024

- **Module:** Database Systems
    - **Lecturer:** John Doe
    - **Room:** 103
    - **Date:** June 20, 2024
    - **Time:** 10:00 - 12:00
    - **Semester:** Winter 2023

#### Teaching Unit: Electrical Engineering
- **Module:** Control Systems
    - **Lecturer:** Jack Black
    - **Room:** 315
    - **Date:** June 18, 2024
    - **Time:** 11:00 - 13:00
    - **Semester:** Summer 2024

- **Module:** Signal Processing
    - **Lecturer:** Emily Rose
    - **Room:** 425
    - **Date:** June 19, 2024
    - **Time:** 13:00 - 15:00
    - **Semester:** Summer 2024

- **Module:** Digital Systems
    - **Lecturer:** Laura Grey
    - **Room:** 220
    - **Date:** June 20, 2024
    - **Time:** 14:00 - 16:00
    - **Semester:** Winter 2023

#### Teaching Unit: Mechanical Engineering
- **Module:** Robotics
    - **Lecturer:** Chris Blue
    - **Room:** 345
    - **Date:** June 18, 2024
    - **Time:** 15:00 - 17:00
    - **Semester:** Summer 2024

- **Module:** Mechatronics
    - **Lecturer:** Alice White
    - **Room:** 455
    - **Date:** June 19, 2024
    - **Time:** 16:00 - 18:00
    - **Semester:** Summer 2024

- **Module:** Thermodynamics
    - **Lecturer:** Luke Green
    - **Room:** 130
    - **Date:** June 20, 2024
    - **Time:** 17:00 - 19:00
    - **Semester:** Winter 2023

### Traceability Matrix for Tests Planning

| **Test Case ID** | **Filter**                       | **Inputs**                                                                 | **Expected Output**                                                                                                                                           |
|------------------|----------------------------------|--------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| TC-01            | Lecturer                         | Lecturer = "Stefan Bente"                                                | Calendar shows only modules taught by Stefan Bente.                                                                                                           |
| TC-02            | Teaching Unit                    | Teaching Unit = "Computer Science"                                       | Calendar shows only modules from the Computer Science teaching unit.                                                                                          |
| TC-03            | Module, Semester                 | Module = "Advanced Machine Learning", Semester = "Summer 2024"           | Calendar shows "Advanced Machine Learning" module details for Summer 2024 semester.                                                                           |
| TC-04            | Teaching Unit, Study Program     | Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)" | Calendar shows modules from "Computer Science" teaching unit under "M.Sc. Digital Sciences (PO1)" study program.                                               |
| TC-05            | Room                             | Room = "0504"                                                            | Calendar shows only modules in Room 0504.                                                                                                                     |
| TC-06            | Lecturer, Room                   | Lecturer = "Stefan Bente", Room = "0504"                                  | Calendar shows modules taught by Stefan Bente in Room 0504.                                                                                                   |
| TC-07            | Teaching Unit, Study Program, Lecturer | Teaching Unit = "Computer Science", Study Program = "M.Sc. Digital Sciences (PO1)", Lecturer = "Stefan Bente" | Calendar shows modules from "Computer Science" teaching unit under "M.Sc. Digital Sciences (PO1)" study program taught by Stefan Bente.                        |
| TC-08            | Teaching Unit, Room              | Teaching Unit = "Computer Science", Room = "103"                          | Calendar shows modules from "Computer Science" teaching unit held in Room 103.                                                                                |

This test data covers a variety of scenarios to ensure that the filters function correctly individually and in combination. Adjust or expand this data based on specific testing needs or additional details.

## Responses with higher temperature
> We tried 2 and 1.5, both ended in nonsense characters. The below response is with temperature = 1.2

Sure! Below is a set of sample data that includes details about teaching units, study programs, semesters, modules, lecturers, and rooms, organized to reflect different filtering scenarios in your calendar management system. Note that this dataset mimics a university setting:

### Teaching Units
1. Computer Science
2. Mathematics
3. Physics

### Study Programs
- **Computer Science**:
    1. B.Sc. Computer Science
    2. M.Sc. Digital Sciences (PO1)
- **Mathematics**:
    1. B.Sc. Mathematics
    2. M.Sc. Applied Mathematics
- **Physics**:
    1. B.Sc. Physics
    2. M.Sc. Theoretical Physics

### Semesters
1. Winter 2023
2. Summer 2024

### Lecturers
1. Stefan Bente
2. Hans Ludwig Stahl
3. Dietlind Zühlke
4. Gernot Heisenberg
5. Irma Lindt
6. Boris Naujoks
7. Matthias Böhmer
8. Christina Werner
9. Konrad Förstner
10. Torsten Klein

### Rooms
1. 3111
2. 149
3. 0502
4. 400
5. 2108
6. 3109
7. 9020

### Modules
#### For Computer Science
1. **Advanced Machine Learning (AML)**
    - Lecturer: Gernot Heisenberg
    - Room: 149
    - Semester: Summer 2024

2. **Network and Distributed Information Systems (NADI)**
    - Lecturer: Hans Ludwig Stahl
    - Room: 3111
    - Semester: Summer 2024

3. **Semantic Web and Multimedia (SGM)**
    - Lecturer: Dietlind Zühlke
    - Room: 0502
    - Semester: Summer 2024

4. **Computational Mathematics (CEX)**
    - Lecturer: Stefan Bente
    - Room: 0504
    - Semester: Summer 2024

5. **System Programming (SPV)**
    - Lecturer: Stefan Karsch
    - Room: 3107
    - Semester: Winter 2023

6. **Ubiquitous Computing (UBICOMP)**
    - Lecturer: Matthias Böhmer
    - Room: 9209
    - Semester: Summer 2024

#### For Mathematics
1. **Linear Algebra (LAL)**
    - Lecturer: Christina Werner
    - Room: 3103
    - Semester: Winter 2023

2. **Stochastic Processes (SPS)**
    - Lecturer: Dietlind Zühlke
    - Room: 9110
    - Semester: Summer 2024

#### For Physics
1. **Quantum Mechanics (QM)**
    - Lecturer: Irene Lindt
    - Room: 3215
    - Semester: Winter 2023

2. **Electrodynamics (ELD)**
    - Lecturer: Boris Naujoks
    - Room: 2100
    - Semester: Summer 2024

### Generated Test Data

**Inputs:**
- **Scenario 1: Single Criterion - Lecturer**
    - Lecturer: Stefan Bente
    - Expected Modules: [Semester: Summer 2024, Module: Computational Mathematics, Room: 0504]

- **Scenario 2: Single Criterion - Room**
    - Room: 149
    - Expected Modules: [Semester: Summer 2024, Module: Advanced Machine Learning, Lecturer: Gernot Heisenberg]

- **Scenario 3: Combined Criteria - Teaching Unit and Study Program**
    - Teaching Unit: Computer Science
    - Study Program: M.Sc. Digital Sciences (PO1)
    - Expected Modules:
        - [Semester: Summer 2024, Module: Advanced Machine Learning, Lecturer: Gernot Heisenberg, Room: 149]
        - [Semester: Summer 2024, Module: Network and Distributed Information Systems, Lecturer: Hans Ludwig Stahl, Room: 3111]
        - [Semester: Summer 2024, Module: Computational Mathematics, Lecturer: Stefan Bente, Room: 0504]
        - [Semester: Summer 2024, Module: Ubiquitous Computing, Lecturer: Matthias Böhmer, Room: 9209]

- **Scenario 4: Multi-Level Complex Criteria**
    - Teaching Unit: Computer Science
    - Study Program: M.Sc. Digital Sciences (PO1)
    - Semester: Summer 2024
    - Lecturer: Stefan Bente
    - Expected Modules:
        - [Semester: Summer 2024, Module: Computational Mathematics, Room: 0504, Lecturer: Stefan Bente]

- **Scenario 5: Lecture and Room**
    - Lecturer: Dietlind Zühlke
    - Room: 0502
    - Expected Modules:
        - [Semester: Summer 2024, Module: Semantic Web and Multimedia, Room: 0502, Lecturer: Dietlind Zühlke]


This data can be used to simulate real-world scenarios in a calendar-type web application that displays university modules based on various filters, ensuring that various filtering mechanisms produce accurate results navigable through the hierarchies given.

