


const compareArrays = (a, b) =>
    a.length === b.length &&
    a.every((element, index) => element === b[index]);

function isSubset(subset, superset) {
    return subset.every(value => superset.includes(value));
}


module.exports = {compareArrays, isSubset};