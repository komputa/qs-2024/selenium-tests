
const { Builder, By, Key, until } = require('selenium-webdriver')

// create an enum for the different filters
const filter = {
    TEACHING_UNIT: 1,
    STUDY_PROGRAM: 2,
    SEMESTER: 3,
    MODULE: 4,
    PROF: 5,
    ROOM: 6
}

async function getFilterValue(driver, dropdown_index) {
    dropdown = await driver.findElement(By.xpath("(//input[@type=\'text\'])[" + dropdown_index + "]"))
    return await dropdown.getAttribute("value")
}

async function getTeachingUnitFilterValue(driver) {
    return getFilterValue(driver, filter.TEACHING_UNIT)
}

async function getStudyProgramFilterValue(driver) {
    return getFilterValue(driver, filter.STUDY_PROGRAM)
}

async function getSemesterFilterValue(driver) {
    return getFilterValue(driver, filter.SEMESTER)
}

async function getModuleFilterValue(driver) {
    return getFilterValue(driver, filter.MODULE)
}

async function getProfFilterValue(driver) {
    return getFilterValue(driver, filter.PROF)
}

async function getRoomFilterValue(driver) {
    return getFilterValue(driver, filter.ROOM)
}


// get All available filters
async function getAllDefaultFilters(driver, dropdown_index) {
    // Focus the select input
    dropdown = await driver.findElement(By.xpath("(//input[@type=\'text\'])[" + dropdown_index + "]"))
    await dropdown.click()

    // Locate the div with id 'SMUI-autocomplete-4-menu'
    const menuDiv = await driver.findElement(By.id('SMUI-autocomplete-' + (dropdown_index - 1) + '-menu'));
    const elements = await menuDiv.findElements(By.tagName('li'));

    // console.log(await menuDiv.getAttribute('innerHTML'));
    names_str = await menuDiv.getText();

    // split the string into an array of names
    names = names_str.split('\n');

    return names;
}

async function getAllTeachingUnits(driver) {
    return getAllDefaultFilters(driver, filter.TEACHING_UNIT);
}

async function getAllStudyPrograms(driver) {
    return getAllDefaultFilters(driver, filter.STUDY_PROGRAM);
}

async function getAllSemesters(driver) {
    return getAllDefaultFilters(driver, filter.SEMESTER);
}

async function getAllModules(driver) {
    return getAllDefaultFilters(driver, filter.MODULE);
}

async function getAllProfs(driver) {
    return getAllDefaultFilters(driver, filter.PROF);
}

async function getAllRooms(driver) {
    return getAllDefaultFilters(driver, filter.ROOM);
}

async function setFilter(driver, dropdown_index, filter_str) {
    // Focus the select input
    dropdown = await driver.findElement(By.xpath("(//input[@type=\'text\'])[" + dropdown_index + "]"))
    await dropdown.click()

    // write the filter string into the input but one character at a time and not the whole string
    for (let i = 0; i < filter_str.length; i++) {
        await dropdown.sendKeys(filter_str[i])
    }

    await driver.sleep(100)
    // click the first element
    // await driver.findElement(By.xpath("//li[contains(.,\'" + filter_str + "\')]")).click();
}





module.exports = { getAllTeachingUnits, getAllStudyPrograms, getAllSemesters, getAllModules, getAllProfs, getAllRooms, setFilter, filter, getTeachingUnitFilterValue, getStudyProgramFilterValue, getSemesterFilterValue, getModuleFilterValue, getProfFilterValue, getRoomFilterValue }
