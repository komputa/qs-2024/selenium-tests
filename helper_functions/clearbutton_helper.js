const { Builder, By, Key, until } = require('selenium-webdriver')

async function getClearButton(driver) {
  return await driver.findElement(By.className("mdc-button mdc-button--outlined mdc-ripple-upgraded"))
}

module.exports = { getClearButton };