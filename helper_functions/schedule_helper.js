const { Builder, By, Key, until } = require('selenium-webdriver')


async function findAllDays(driver) {
    return await driver.findElements(By.className('fc_day'))
}

async function findMonday(driver) {
    return await driver.findElement(By.className('fc-day-mon'))
}

async function findTuesday(driver) {
    return await driver.findElement(By.className('fc-day-tue'))
}

async function findWednesday(driver) {
    return await driver.findElement(By.className('fc-day-wed'))
}

async function findThursday(driver) {
    return await driver.findElement(By.className('fc-day-thu'))
}

async function findFriday(driver) {
    return await driver.findElement(By.className('fc-day-fri'))
}

async function findSaturday(driver) {
    return await driver.findElement(By.className('fc-day-sat'))
}


async function findAllEvents(driver) {
    events = await driver.findElements(By.className('fc-event-main'))

    // get first div in each event
    for (let i = 0; i < events.length; i++) {
        events[i] = await events[i].findElement(By.tagName('div'))
    }
    return events
}

async function getEventName(event)
{
    return await event.findElement(By.tagName('span')).getText()
}

async function getEventProf(event)
{
    room_prof_div =  await event.findElement(By.tagName('div'))
    room_prof_div_spans = await room_prof_div.findElements(By.tagName('span'))
    prof_span = room_prof_div_spans[2]
    prof_str = await prof_span.getText()
    // split \n and return the second element
    return prof_str.split('\n')[1]
}

async function getEventRoom(event)
{
    room_prof_div =  await event.findElement(By.tagName('div'))
    room_prof_div_spans = await room_prof_div.findElements(By.tagName('span'))
    prof_span = room_prof_div_spans[0]
    prof_str = await prof_span.getText()
    // split \n and return the second element
    return prof_str.split('\n')[1]
}

async function getAllScheduleModuleNames(driver) {
    const all_modules = await findAllEvents(driver)
    module_names = []
    for (let i = 0; i < all_modules.length; i++) {
        module_names.push(await getEventName(all_modules[i]))
    }
    
    return module_names 
}

async function getAllScheduleProfs(driver) {
    const all_modules = await findAllEvents(driver)
    prof_names = []
    for (let i = 0; i < all_modules.length; i++) {
        prof_names.push(await getEventProf(all_modules[i]))
    }
    
    return prof_names 
}


async function getAllScheduleRooms(driver) {
    const all_modules = await findAllEvents(driver)
    prof_names = []
    for (let i = 0; i < all_modules.length; i++) {
        prof_names.push(await getEventRoom(all_modules[i]))
    }
    
    return prof_names 
}


module.exports = { getAllScheduleModuleNames, getAllScheduleProfs, getAllScheduleRooms, findAllDays, findMonday, findTuesday, findWednesday, findThursday, findFriday, findSaturday}