require('dotenv').config();

exports.getBrowser = function() {
    return process.env.BROWSER ?? 'firefox';
}
