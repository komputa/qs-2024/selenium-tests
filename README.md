# Selenium Tests

`npm i`

`npm run test` for all tests

## Changing the browser to run the tests in
Edit your `.env` file (or create it by copying `.env.example`) to change the browser to run the tests in.
The default is `firefox`.
