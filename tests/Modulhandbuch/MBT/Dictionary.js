const {By} = require("selenium-webdriver");
const assert = require("assert");
const actionDictionary = {
    e_Action: {
        id: 'actionID',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_WebsiteOeffnen: {
        id: 'websiteStart',
        command: async (driver) => {
            await driver.get("http://advp44.gm.fh-koeln.de:8080/");
            await driver.manage().setTimeouts({implicit: 3000});
        }
    },
    e_AuswahlLogin: {
        id: 'e_AuswahlLogin',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_LoginMItRichtigenCreds: {
        id: 'e_LoginMItRichtigenCreds',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_Abmelden: {
        id: 'e_Abmelden',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_AuswahlModul: {
        id: 'e_AuswahlModul',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_ZurueckNachMeineModule: {
        id: 'e_ZurueckNachMeineModule',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_NeuesModul: {
        id: 'e_NeuesModul',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_SpeichernFalscherEingaben: {
        id: 'e_SpeichernFalscherEingaben',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_SpeichernMitErforderlichenAngaben: {
        id: 'e_SpeichernMitErforderlichenAngaben',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_WeiterleitungMeineModule: {
        id: 'e_WeiterleitungMeineModule',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_LoginMitFalschenCreds: {
        id: 'e_LoginMitFalschenCreds',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
    e_NewEdge: {
        id: 'e_NewEdge',
        command: async (driver) => {
            // Selenium Befehle zur Ausführung der Aktion
        },
    },
};

const stateDictionary = {
    v_State: {
        id: 'stateID',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_ModulAnsicht: {
        id: 'v_ModulAnsicht',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_Login: {
        id: 'v_Login',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_MeineModule: {
        id: 'v_MeineModule',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_AnsichtModul: {
        id: 'v_AnsichtModul',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_ModulErstellenAnsicht: {
        id: 'v_ModulErstellenAnsicht',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
    v_NeuesModulInMeineModule: {
        id: 'v_NeuesModulInMeineModule',
        command: async (driver) => {
            // Selenium Befehle zur Überprüfung, ob wir uns im erwarteten State befinden
        },
    },
}

async function executeAction(driver, action) {
    if (action) {
        await action.command(driver);
        console.log(`Aktion ${action.id} ausgeführt.`);
    } else {
        console.error(`Aktion nicht gefunden.`);
    }
}

async function assertState(driver, state){
    if(state){
        await state.command(driver);
        console.log(`State ${state.id} asserted.`)
    } else {
        console.error(`State nicht gefunden.`)
    }
}

module.exports = {actionDictionary, stateDictionary, executeAction, assertState};