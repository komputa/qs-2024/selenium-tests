const { Builder, By, Key, until } = require('selenium-webdriver')
const {actionDictionary, stateDictionary, executeAction, assertState} = require('./Dictionary.js')

describe('Modulbuchansicht und erstellung', function() {
    this.timeout(30000)
    let driver
    let vars
    beforeEach(async function() {
        driver = await new Builder().forBrowser(require('../../../envHelpers').getBrowser()).build()
        // Setze die Gerätegröße auf eine mobile Ansicht (Bsp. iPhone XR)
        let x = 0;
        let y = 0;
        let width = 414;
        let height = 896;
        await driver.manage().window().setSize({x,y,width,height});

        // Warte auf das Laden der Webseite
        await driver.wait(until.elementLocated(By.css('body')));
        vars = {}
    })
    afterEach(async function() {
        await driver.quit();
    })
    it('Testpfadgenerator random(edge-coverage=100)', async function() {
        await executeAction(driver, actionDictionary.e_WebsiteOeffnen);
        await assertState(driver, stateDictionary.v_ModulAnsicht);
        await executeAction(driver, actionDictionary.e_AuswahlLogin);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_SpeichernFalscherEingaben);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_SpeichernFalscherEingaben);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_SpeichernFalscherEingaben);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMitFalschenCreds);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_SpeichernMitErforderlichenAngaben);
        await assertState(driver, stateDictionary.v_NeuesModulInMeineModule);
        await executeAction(driver, actionDictionary.e_WeiterleitungMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMitFalschenCreds);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMitFalschenCreds);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_Abmelden);
        await assertState(driver, stateDictionary.v_Login);
        await executeAction(driver, actionDictionary.e_LoginMItRichtigenCreds);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_AuswahlModul);
        await assertState(driver, stateDictionary.v_AnsichtModul);
        await executeAction(driver, actionDictionary.e_ZurueckNachMeineModule);
        await assertState(driver, stateDictionary.v_MeineModule);
        await executeAction(driver, actionDictionary.e_NeuesModul);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
        await executeAction(driver, actionDictionary.e_NewEdge);
        await assertState(driver, stateDictionary.v_ModulErstellenAnsicht);
    })
})
