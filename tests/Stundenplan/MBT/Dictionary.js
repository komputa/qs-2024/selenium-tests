const { By } = require("selenium-webdriver");
const assert = require("assert");

const startPage = "http://advp44.gm.fh-koeln.de:8080/";

const filter = require('../../../helper_functions/filter_helper')
const clearbutton_helper = require('../../../helper_functions/clearbutton_helper')

const actionDictionary = {
    e_WebsiteOeffnen: {
        id: 'websiteStart',
        command: async (driver) => {
            await driver.get(startPage);
            await driver.manage().setTimeouts({ implicit: 3000 });
        }
    },
    e_AufDetailsKlicken: {
        id: 'eAufDetailsKlicken',
        command: async (driver) => {
            // TODO: Click auf details link in the module modal
            const openedModal = await driver.findElement(By.className("mdc-dialog--open"))
            const detailsButton = await openedModal.findElement(By.tagName("a"))
            await detailsButton.click()
        }
    },
    e_AufModulImKalenderKlicken: {
        id: 'eAufModulImKalenderKlicken',
        command: async (driver) => {
            // Get first shown module in the calendar and click it
            const collection = await driver.findElements(By.className("fc-event"))
            const firstElement = collection[0];
            await firstElement.click();
        }
    },
    e_AutomatischeFilterung: {
        id: 'eAutomatischeFilterung',
        command: async (_) => {
            // This does nothing, this is an automatic, implicit action that happens after selecting a filter
        }
    },
    e_Clear_Filter: {
        id: 'eClearFilter',
        command: async (driver) => {
            const button = await clearbutton_helper.getClearButton(driver)
            await button.click()
        }
    },
    e_DatenInLecturerFilterEintragen: {
        id: 'eDatenInLecturerFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.PROF, "Raphaela Groten")
        }
    },
    e_DatenInModulFilterEintragen: {
        id: 'eDatenInModulFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.MODULE, "Einführungsprojekt in die Informatik (EPI)")
        }
    },
    e_DatenInRaumFilterEintragen: {
        id: 'eDatenInRaumFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.ROOM, "0503 (KI-Labor LC6)")
        }
    },
    e_DatenInSemesterFilterEintragen: {
        id: 'eDatenInSemesterFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.SEMESTER, "1. Semester")
        }
    },
    e_DatenInStudyProgrammFilterEintragen: {
        id: 'eDatenInStudyProgrammFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.STUDY_PROGRAM, "B.Sc. Computer Science (PO2)")
        }
    },
    e_DatenInTeachingUnitFilterEintragen: {
        id: 'eDatenInTeachingUnitFilterEintragen',
        command: async (driver) => {
            await filter.setFilter(driver, filter.filter.TEACHING_UNIT, "Computer Science")
        }
    },
    e_ZurueckNavigieren: {
        id: 'eZurueckNavigieren',
        command: async (driver) => {
            await driver.navigate().back();
            const closeButtons = await driver.findElements(By.className("mdc-dialog__close"));
            await closeButtons[0].click();
        }
    },
};

const stateDictionary = {
    v_AnsichtGefiltert: {
        id: 'vAnsichtGefiltert',
        command: async (driver) => {
            // Assert that the view is filtered
            const teaching_unit_filtered = await filter.getTeachingUnitFilterValue(driver)
            const study_program_filtered = await filter.getStudyProgramFilterValue(driver)
            const semester_filtered = await filter.getSemesterFilterValue(driver)
            const module_filtered = await filter.getModuleFilterValue(driver)
            const prof_filtered = await filter.getProfFilterValue(driver)
            const room_filtered = await filter.getRoomFilterValue(driver)
            const filtered = teaching_unit_filtered || study_program_filtered || semester_filtered || module_filtered || prof_filtered || room_filtered
            assert(filtered)
        }
    },
    v_LecturerFilterGefuellt: {
        id: 'vLecturerFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getProfFilterValue(driver)
                assert(value)
            }
        }
    },
    v_ModuleFilterGefuellt: {
        id: 'vModuleFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getModuleFilterValue(driver)
                assert(value)
            }
        }
    },
    v_ModulModalGeoeffnet: {
        id: 'vModulModalGeoeffnet',
        command: async (driver) => {
            // TODO: Assert that there is a open module modal (class = mdc-dialog--open)
            const openedModal = await driver.findElement(By.className("mdc-dialog--open"))
            assert(openedModal)
        }
    },
    v_RaumFilterGefuellt: {
        id: 'vRaumFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getRoomFilterValue(driver)
                assert(value)
            }
        }
    },
    v_SemesterFilterGefuellt: {
        id: 'vSemesterFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getSemesterFilterValue(driver)
                assert(value)
            }
        }
    },
    v_StudyProgrammFilterGefuellt: {
        id: 'vStudyProgrammFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getStudyProgramFilterValue(driver)
                assert(value)
            }
        }
    },
    v_TeachingUnitFilterGefuellt: {
        id: 'vTeachingUnitFilterGefuellt',
        command: async (driver) => {
            {
                const value = await filter.getTeachingUnitFilterValue(driver)
                assert(value)
            }
        }
    },
    v_UngefilterteAnsicht: {
        id: 'vUngefilterteAnsicht',
        command: async (driver) => {
            // Assert that all filter fields are empty
            {
                const value = await filter.getTeachingUnitFilterValue(driver)
                assert(!value)
            }
            {
                const value = await filter.getStudyProgramFilterValue(driver)
                assert(!value)
            }
            {
                const value = await filter.getSemesterFilterValue(driver)
                assert(!value)
            }
            {
                const value = await filter.getModuleFilterValue(driver)
                assert(!value)
            }
            {
                const value = await filter.getProfFilterValue(driver)
                assert(!value)
            }
            {
                const value = await filter.getRoomFilterValue(driver)
                assert(!value)
            }
        }
    },
    v_WeitergeleitetAufModulinfos: {
        id: 'vWeitergeleitetAufModulinfos',
        command: async (driver) => {
            // Asser that we don't see the port 8080 in the URL and thus are redirected
            const url = await driver.getCurrentUrl();
            assert(!url.includes("8080"));
        }
    },
}

async function executeAction(driver, action) {
    if (action) {
        await action.command(driver);
        console.log(`Aktion ${action.id} ausgeführt.`);
    } else {
        console.error(`Aktion ${action} nicht gefunden.`);
    }
}

async function assertState(driver, state) {
    if (state) {
        await state.command(driver);
        console.log(`State ${state.id} asserted.`)
    } else {
        console.error(`State ${state} nicht gefunden.`)
    }
}

module.exports = { actionDictionary, stateDictionary, executeAction, assertState };
